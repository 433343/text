//
//  AppDelegate.h
//  Config
//
//  Created by pdid on 2017/7/13.
//  Copyright © 2017年 pdid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

